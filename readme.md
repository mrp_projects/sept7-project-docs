# Sept7 REST API Documentation

_Version 0.4.0_

---



## Authentication Service

The API will provide methods for authenticating a user to the CMS via username/password (which use Craft's native internal member system), or by Facebook token (which will map a Facebook user to a native Craft user).

Submitting a valid credential will cause the API to return the user's User Identity (which includes the User Token):

```json
{
	"user_id": 1,
	"email": "picard@starfleet.ufp",
	"first_name": "Jean-Luc",
	"last_name": "Picard",
	"full_name": "Jean-Luc Picard",
	"token": "1z2y3x4wToken999blahblahblah",
	"token_expiry": "2016-04-22T07:77:00+2:00"
}
```

The client may retain the token for later user.

**_Requests to any other (non-Authentication) API service will be authenticated by including the `user_token` as a parameter to the request._**

Upon submission of an invalid credential, the API will return an error message:

```json
{
	"error": "I'm sorry, I can't do that, Dave."
}
```

Methods for Authentication Service are:

### `POST` /auth/password

* `login_name`: The user's email address or username
* `password`: The password provided by the user

### `POST` /auth/facebook

* `facebook_token`: The user token provided by Facebook

_If a Facebook authentication is submitted for a user that does not yet exist (i.e. the email of the Facebook account does not match the email of a user in the system), the new user will be created and then authenticated._

### Registering a New User

Use Craft's native [`users/saveUser`](https://craftcms.com/classreference/controllers/UsersController#actionSaveUser-detail) action to register a new user.

Once confirmation is given that the user was successfully created, a token can be obtained by submitting the same username/password to `/auth/password`. (The new user may need to confirm their account, depending on the current Craft config.) 


## Tokens Service

Once a client acquires a User Token, it may be desirable extend the token or to discard the token.

The Token Service provides these methods:

### `POST` /tokens/extend

* `user_token`: The User Token

Extends the token (if allowed) and returns a success message:

```json
{
	"success": true,
	"token": "1z2y3x4wToken999blahblahblah",
	"token_expiry": "2016-04-22T07:77:00"
}
```

Or, if the token cannot be extended, returns an error message:

```
{
	"error": "Token cannot be extended."
}
```

### `POST` /tokens/expire

* `user_token`: The User Token

Expires the token (if allowed) and returns a success message as confirmation:

```json
{
	"success": true
}
```

Or, if the token cannot be expired, returns an error message:

```json
{
	"error": "That token doesn't exist."
}
```



## Subscriptions Service

The Subscription Service is responsible for dealing with information about the user's subscription to the magazine.

### `POST` /subscriptions/register

* `user_token`: The User Token
* `receipt_data`: The base64-encoded receipt data from the Apple transaction

Returns the receipt data for confirmation:

```
{
	"receipt_data": ...
}
```

Or, if there was an error, returns an error message:

```
{
	"error": "The subscription could not be saved."
}
```



## Entries Service

The Entries Service is responsible for serving up News entries, according to the user's subscription.

### `POST` /entries/find

If you wish to authenticate a user for the request, provide a User Token as usual:

* `user_token`: The User Token

You can filter the entries which will be returned:

* `filter[section]`: (int) The handle of a section; Only articles belonging to this section will be returned.

* `filter[slug]`: (string) The article's URL slug; Only the article matching this slug will be returned.

* `filter[author]`: (int) The ID of an author; Only entries assigned to the matching author will be returned.

* `filter[relatedTo]`: (int) The ID of a category/element, or a comma-separated list of IDs; Only entries related to matching categories/elements will be returned.

* `filter[search]`: A search query string; Articles matching the search query will be returned.

By default, the first page of 20 entries is returned. You can manipulate the pagination with additional parameters:

* `entries_per_page`: (int)
* `page`: (int)

_Page numbers are 1-indexed._

A successful response will include a `data` property containing an array of Entry objects and a `meta` property containing pagination information:

```json
{
   "data" : NewsEntry[],
   "meta": {
      "pagination": {
         "total": 1042,
         "count": 20,
         "per_page": 20,
         "current_page": 1,
         "total_pages": 61,
         "links": {
            "next": (URL)
         }
      }
   }
}
```


## Content Models

The content models are set up as follows:

```
NewsEntry
{
	"id": (int),
	"slug": (string),
	"title": (string),
	"url": (string),
	"postDate": Date/Time,
	"authors": Author[],
	"featuredImageThumbnailUrl": (string),
	"featuredImageUrl": (string),
	"featuredImageSourceUrl": (string),
	"storySections": Category[],
	"columnSections": Category[],
}

Author
{
	"id": (int),
	"name": (string)
}

Category
{
	"id": (int),
	"slug": (string),
	"title": (string),
	"parentId": (int)
}
```




# Sept7 Craft Services Documentation

_Version 0.1.0_

---



## Subscriptions Service

The Subscriptions service provides information about a particular user's access to articles.

A user is granted access to an article if:
* They have purchased the article directly, or
* They have an active subscription in Chargebee

The Subscriptions service provides these template methods:

### `userHasActiveSubscription(UserModel $user)`

Accepts a single parameter, a _UserModel_ of the user to test. (If no user is specified, the `currentUser` will be used as a default.)

Returns a boolean, whether an active subscription can be found for this user in Chargebee. 

```twig
{% set picard = craft.users.id(42).first %}

{% if craft.sept_subscriptions.userHasActiveSubscription( picard ) %}
	...
{% endif %}
```

```twig
{% if craft.sept_subscriptions.userHasActiveSubscription %}
	...
{% endif %}
```

### `userCanAccessEntry(EntryModel $entry, UserModel $user)`

Accepts two parameters: an _EntryModel_ and a _UserModel_. (If no user is specified, the `currentUser` will be used as a default.)

Returns a boolean, whether the user has access to the specified entry. 

```twig
{% set entry = craft.entries.slug('homepage').first %}
{% set picard = craft.users.id(42).first %}

{% if craft.sept_subscriptions.userCanAccessEntry( entry, picard ) %}
	...
{% endif %}
```

```twig
{% if craft.sept_subscriptions.userCanAccessEntry (entry ) %}
	...
{% endif %}
```
